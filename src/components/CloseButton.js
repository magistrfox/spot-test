import React from "react";
import styles from '../styles/CloseButton.module.css';

const CloseButton = ({addClass, handler, screen}) => {
  return (
    <div onClick={() => handler(screen)} className={`${styles.close} ${addClass}`}>
      <img src="./assets/close.svg" alt="close" />
    </div>
  );
};

export default CloseButton;
