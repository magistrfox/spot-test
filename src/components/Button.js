import React from "react";
import styles from "../styles/Button.module.css";

const Button = ({ addClass, disabled, text, handler, coords, screen }) => {
  return (
    <button
      onClick={() => handler(screen, coords)}
      className={`${styles.btn} ${addClass}`}
      disabled={disabled}
    >
      {text}
    </button>
  );
};

export default Button;
