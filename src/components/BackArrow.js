import React from "react";
import styles from "../styles/BackArrow.module.css";

const BackArrow = ({handler, screen}) => {
  return (
    <div onClick={() => handler(screen)} className={styles.box}>
      <img src="./assets/back-arrow.svg" alt="go-back" />
    </div>
  );
};

export default BackArrow;
