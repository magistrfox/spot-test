import React from "react";

const Marker = () => {
  return (
    <div style={{ width: "28px", height: "42px", position: "relative" }}>
      <img
        style={{
          width: "100%",
          height: "100%",
          objectFit: "cover",
          position: "absolute",
          transform: 'translate(-50%, -50%)'
        }}
        src="./assets/marker.svg"
        alt="marker"
      />
    </div>
  );
};

export default Marker;
