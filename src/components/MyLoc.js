import React from "react";
import styles from '../styles/MyLoc.module.css';

const MyLoc = ({addClass, handler}) => {
  return (
    <div onClick={() => handler()} className={`${styles.loc} ${addClass}`}>
      <img src="./assets/myLoc.svg" alt="location-logo" />
      <span>MyLoc</span>
    </div>
  );
};

export default MyLoc;
