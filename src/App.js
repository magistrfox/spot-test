import React, { useState, useEffect } from "react";
import "./App.css";
import TapToScreen from "./screens/TapToScreen";
import StartScreen from "./screens/StartScreen";
import MainScreen from "./screens/MainScreen";
import SendScreen from "./screens/SendScreen";
import PointsScreen from "./screens/PointsScreen";
import CloseScreen from "./screens/CloseScreen";

const App = () => {
  const [visibility, setVisibility] = useState({
    tapToScreen: true,
    startScreen: false,
    mainScreen: false,
    sendScreen: false,
    pointsScreen: false,
    closeScreen: false,
  });

  const [coords, setCoords] = useState({
    lat: null,
    lng: null,
  });

  const [trackId, setTrackId] = useState(null);

  const visibilityHandler = (currScreen, crds) => {
    //    screen prop is not what screen came to func but what screen we should see
    const newVisibility = {};
    for (let screen in visibility) {
      newVisibility[screen] = false;
      if (screen === currScreen) {
        newVisibility[screen] = true;
      }
      if (screen === "sendScreen") {
        setCoords(crds);
      }
    }
    setVisibility(newVisibility);
    localStorage.setItem("view", JSON.stringify(newVisibility));
  };

  useEffect(() => {
    setVisibility(JSON.parse(localStorage.getItem("view")) || visibility);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      {visibility.tapToScreen && <TapToScreen handler={visibilityHandler} />}
      {visibility.startScreen && <StartScreen handler={visibilityHandler} />}
      {visibility.mainScreen && (
        <MainScreen
          handler={visibilityHandler}
          setTrackId={setTrackId}
          trackId={trackId}
        />
      )}
      {visibility.sendScreen && (
        <SendScreen
          trackId={trackId}
          handler={visibilityHandler}
          coords={coords}
        />
      )}
      {visibility.pointsScreen && (
        <PointsScreen handler={visibilityHandler} trackId={trackId} />
      )}
      {visibility.closeScreen && <CloseScreen handler={visibilityHandler} />}
    </div>
  );
};

export default App;
