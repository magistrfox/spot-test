import React from "react";
import styles from "../styles/CloseScreen.module.css";

const CloseScreen = ({handler}) => {
  return (
    <div className={styles.box}>
      <div className={styles.dialog}>
        <span>Уверены, что хотите выйти?</span>
        <div className={styles.choose}>
          <div onClick={() => handler('tapToScreen')}>Да</div>
          <div onClick={() => handler('mainScreen')}>Нет</div>
        </div>
      </div>
    </div>
  );
};

export default CloseScreen;
