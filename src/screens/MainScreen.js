import React, { useState } from "react";
import styles from "../styles/MainScreen.module.css";
import CloseButton from "../components/CloseButton";
import MyLoc from "../components/MyLoc";
import Marker from "../components/Marker";
import GoogleMapReact from "google-map-react";
import Button from "../components/Button";

const MainScreen = ({ handler, setTrackId, trackId }) => {
  const [coords, setCoords] = useState({
    lat: 55.748576,
    lng: 38.620164,
  });

  const success = (pos) => {
    const crd = pos.coords;
    setCoords({ lat: crd.latitude, lng: crd.longitude });
  };

  const error = (err) => {
    console.log(err);
  };

  const myLocHandler = () => {
    navigator.geolocation.getCurrentPosition(success, error);
  };

  const clickMapHandler = ({ lat, lng }) => {
    setCoords({
      lat,
      lng,
    });
  };

  const handleTrack = () => {
    (async () => {
      try {
        const POST = await fetch("/start_track", {
          method: "POST",
          headers: {
            Connection: "keep-alive",
            "Content-Type": "application/json",
          },
        });
        const res = await POST.json();
        setTrackId(res.track_id);
      } catch (e) {
        console.error(e);
      }
    })();
  };

  return (
    <div className={styles.box}>
      <CloseButton
        handler={handler}
        screen="closeScreen"
        addClass={styles.close}
      />
      <MyLoc handler={myLocHandler} addClass={styles.loc} />
      <span className={styles.text}>Выберите вашу геопозицию</span>
      <div className={styles.map}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyAMMy0U0VWPKajlwKtlO_IfdBAwQHuY_Go" }}
          defaultZoom={11}
          center={{
            lat: coords.lat,
            lng: coords.lng,
          }}
          yesIWantToUseGoogleMapApiInternals
          onClick={clickMapHandler}
        >
          <Marker lat={coords.lat} lng={coords.lng} />
        </GoogleMapReact>
      </div>
      {!trackId ? (
        <span onClick={handleTrack} className={styles.track}>
          Создать трек
        </span>
      ) : (
        <span className={styles.track_complete}>Трек создан</span>
      )}
      <Button
        addClass={styles.select}
        handler={handler}
        screen="sendScreen"
        coords={coords}
        text="Выбрать"
        disabled={!trackId ? true : false}
      />
    </div>
  );
};

export default MainScreen;
