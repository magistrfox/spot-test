import React from "react";
import styles from "../styles/TapToScreen.module.css";

const TapToScreen = ({ handler }) => {
  return (
    <>
      {
        <div onClick={() => handler('startScreen')} className={styles.box}>
          <span>Нажмите на экран</span>
        </div>
      }
    </>
  );
};

export default TapToScreen;
