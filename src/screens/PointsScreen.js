import React, { useEffect, useState } from "react";
import styles from "../styles/PointsScreen.module.css";
import MyLoc from "../components/MyLoc";
import BackArrow from "../components/BackArrow";

const PointScreen = ({ handler, trackId }) => {
  const [points, setPoints] = useState([]);
  const [currPoint, setCurrPoint] = useState({
    order_id: "",
    latitude: "",
    longitude: ""
  });

  useEffect(() => {
    (async () => {
      const URL = `/get_track/${trackId}`;
      try {
        const GET = await fetch(URL);
        const res = await GET.json();
        const confirmOrder = res.sort((a, b) => a.order_id - b.order_id);
        setPoints(confirmOrder);
      } catch (e) {
        console.error(e);
      }
    })();
  });

  const pointHandler = async point => {
    setCurrPoint(point);
    setTimeout(
      () =>
        setCurrPoint({
          order_id: "",
          latitude: "",
          longitude: ""
        }),
      1000
    );
  };

  return (
    <div className={styles.box}>
      <MyLoc addClass={styles.loc} />
      <BackArrow handler={handler} screen="sendScreen" />
      <span className={styles.text}>Проверьте добавленные точки</span>
      <span className={styles.text_check}>
        Точка {currPoint.order_id !== "" && currPoint.order_id + 1}
      </span>
      <div className={styles.form_box}>
        <label>
          <input type="text" disabled value={currPoint.latitude} />
          <span>Широта</span>
        </label>
        <label>
          <input type="text" disabled value={currPoint.longitude} />
          <span>Долгота</span>
        </label>
      </div>
      <span className={styles.text_choose}>Выберите добавленную точку</span>
      <div className={styles.point_box}>
        {points &&
          points.map(point => (
            <div
              key={point.order_id}
              className={styles.point}
              onClick={() => pointHandler(point)}
            >
              Точка {point.order_id + 1}
            </div>
          ))}
      </div>
      <div onClick={() => handler("mainScreen")} className={styles.add}>
        Добавить еще
      </div>
    </div>
  );
};

export default PointScreen;
