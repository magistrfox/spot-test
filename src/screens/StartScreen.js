import React from "react";
import styles from "../styles/StartScreen.module.css";

const StartSreen = ({ handler }) => {
  return (
    <>
      <div className={styles.start}>
        <span onClick={() => handler('mainScreen')}>Начать</span>
      </div>
    </>
  );
};

export default StartSreen;
