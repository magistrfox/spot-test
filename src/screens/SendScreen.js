import React, { useState, useEffect } from "react";
import styles from "../styles/SendScreen.module.css";
import MyLoc from "../components/MyLoc";
import BackArrow from "../components/BackArrow";

const SendScreen = ({ coords = {}, handler, trackId }) => {
  const [localCoords, setLocalCoords] = useState({
    longitude: coords.lng,
    latitude: coords.lat,
    track_id: trackId,
  });

  const submitHandler = (e) => {
    e.preventDefault();
    handler("pointsScreen");
    (async () => {
      try {
        const POST = await fetch("/save_track_point", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: localStorage.getItem("coords"),
        });
        const res = await POST.json();
        console.log(res);
      } catch (e) {
        console.error(e);
      }
    })();
  };

  const inputHandler = (e) => {
    setLocalCoords({
      ...localCoords,
      [e.target.name]: e.target.value,
    });
    localStorage.setItem(
      "coords",
      JSON.stringify({
        ...localCoords,
        [e.target.name]: e.target.value,
      })
    );
  };

  useEffect(() => {
    if (localCoords.longitude && localCoords.latitude) {
      localStorage.setItem("coords", JSON.stringify(localCoords));
    }
    setLocalCoords(JSON.parse(localStorage.getItem("coords")));
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={styles.box}>
      <MyLoc addClass={styles.loc} />
      <BackArrow handler={handler} screen="mainScreen" />
      <span className={styles.text}>Отправьте выбранную геопозицию</span>
      <form onSubmit={(e) => submitHandler(e)}>
        <label className={styles.form_box}>
          Широта{" "}
          <input
            name="latitude"
            type="text"
            onChange={(e) => inputHandler(e)}
            value={localCoords.latitude}
          />
        </label>
        <label className={styles.form_box}>
          Долгота
          <input
            name="longitude"
            type="text"
            onChange={(e) => inputHandler(e)}
            value={localCoords.longitude}
          />
        </label>
        <button className={styles.submit} type="submit">
          Отправить
        </button>
      </form>
    </div>
  );
};

export default SendScreen;
